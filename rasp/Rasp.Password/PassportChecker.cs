﻿using System;

namespace Rasp.Password
{
    public class PassportChecker
    {
        private const int PasswordLength = 10;

        public bool IsPassportCorrect(string passport)
        {
            string passportNumbers = passport.Trim().Replace(" ", string.Empty);
            return passportNumbers.Length == PasswordLength;
        }

        public string GetErrorMessage(string passport)
        {
            if (IsPassportCorrect(passport))
                throw new InvalidOperationException("Паспорт введён коррентно");

            if (passport.Trim() == string.Empty)
                return "Введите серию и номер паспорта";

            return "Неверный формат серии или номера паспорта";
        }
    }
}
