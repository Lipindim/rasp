﻿using System;
using System.Windows.Forms;

namespace Rasp.Password
{
    public partial class PassportForm : Form
    {
        private VotingDataProvider _votingDataProvider;
        private PassportChecker _passportChecker;

        public PassportForm()
        {
            InitializeComponent();

            Cryptography cryptography = new Cryptography();
            _votingDataProvider = new VotingDataProvider(cryptography);
            _passportChecker = new PassportChecker();

        }

        private void CheckPassport(object sender, EventArgs e)
        {
            string passport = _passportTextbox.Text;
            if (!_passportChecker.IsPassportCorrect(passport))
            {
                _textResult.Text = _passportChecker.GetErrorMessage(passport);
                return;
            }

            try
            {
                string votingStatus = _votingDataProvider.GetVotingStatus(passport);
                _textResult.Text = votingStatus;
            }
            catch (Exception exception)
            {
                _textResult.Text = exception.Message;
            }
        }

    }
}
