﻿
namespace Rasp.Password
{
    partial class PassportForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._passportTextbox = new System.Windows.Forms.TextBox();
            this._textResult = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _passportTextbox
            // 
            this._passportTextbox.Location = new System.Drawing.Point(0, 0);
            this._passportTextbox.Name = "_passportTextbox";
            this._passportTextbox.Size = new System.Drawing.Size(100, 23);
            this._passportTextbox.TabIndex = 0;
            // 
            // _textResult
            // 
            this._textResult.AutoSize = true;
            this._textResult.Location = new System.Drawing.Point(62, 157);
            this._textResult.Name = "_textResult";
            this._textResult.Size = new System.Drawing.Size(44, 15);
            this._textResult.TabIndex = 1;
            this._textResult.Text = "Results";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this._textResult);
            this.Controls.Add(this._passportTextbox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _passportTextbox;
        private System.Windows.Forms.Label _textResult;
    }
}

