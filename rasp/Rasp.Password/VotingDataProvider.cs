﻿using Microsoft.Data.Sqlite;
using System;
using System.Data;
using System.IO;
using System.Reflection;

namespace Rasp.Password
{
    public class VotingDataProvider
    {
        private const string DatabaseFilename = "\\db.sqlite";
        private const int AccessColumn = 1;

        private Cryptography _cryptography;

        public VotingDataProvider(Cryptography cryptography)
        {
            _cryptography = cryptography;
        }

        public string DatabasePath => Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + DatabaseFilename;


        public string GetVotingStatus(string passport)
        {
            if (!File.Exists(DatabasePath))
                throw new InvalidOperationException("Файл db.sqlite не найден. Положите файл в папку вместе с exe.");

            string command = CreateCommand(passport);
            string connectionString = GetConnectionString();
            SqliteConnection connection = new SqliteConnection(connectionString);
            connection.Open();
            SQLiteDataAdapter sqLiteDataAdapter = new SQLiteDataAdapter(new SQLiteCommand(command, connection));
            DataTable passportDataTable = new DataTable();
            sqLiteDataAdapter.Fill(passportDataTable);
            string result = InterpretVotingData(passportDataTable, passport);
            connection.Close();

            return result;
        }

        private string InterpretVotingData(DataTable passportDataTable, string passport)
        {
            if (passportDataTable.Rows.Count == 0)
                return $"Паспорт «{passport}» в списке участников дистанционного голосования НЕ НАЙДЕН";

            if (IsAccessGranted(passportDataTable))
                return $"По паспорту «{passport}» доступ к бюллетеню на дистанционном электронном голосовании ПРЕДОСТАВЛЕН";
            else
                return $"По паспорту «{passport}» доступ к бюллетеню на дистанционном электронном голосовании НЕ ПРЕДОСТАВЛЯЛСЯ";

        }

        private bool IsAccessGranted(DataTable passportDataTable)
        {
            return Convert.ToBoolean(passportDataTable.Rows[0].ItemArray[AccessColumn]);
        }

        private string CreateCommand(string rawData)
        {
            string sha256Hash = _cryptography.ComputeSha256Hash(rawData);
            string commandText = $"select * from passports where num='{sha256Hash}' limit 1;";
            return commandText;
        }

        private string GetConnectionString()
        {
            return $"Data Source={DatabasePath};";
        }
    }
}
