﻿namespace Rasp.Logger
{
    public interface ILogger
    {
        void WriteError(string message);
    }
}
