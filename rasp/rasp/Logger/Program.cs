﻿namespace Rasp.Logger
{
    class Program
    {
        static void Main(string[] args)
        {
            var logToFilePathfinder = new Pathfinder(new FileLogWriter());
            var logToConsolePathfinder = new Pathfinder(new ConsoleLogWritter());
            var secureLogToFilePathfinder = new Pathfinder(new SecureLogger(new FileLogWriter()));
            var secureLogToConsolePathfinder = new Pathfinder(new SecureLogger(new ConsoleLogWritter()));
            var lotToConsoleAndSecureLogToFilePathfiner = new Pathfinder(
                new CombineLogger(
                    new ConsoleLogWritter(),
                    new SecureLogger(new FileLogWriter()
                    )));
        }
    }
}
