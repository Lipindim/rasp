﻿using System.IO;

namespace Rasp.Logger
{
    public class FileLogWriter : ILogger
    {
        public void WriteError(string message)
        {
            File.WriteAllText("log.txt", message);
        }
    }
}