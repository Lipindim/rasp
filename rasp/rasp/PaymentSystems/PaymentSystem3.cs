﻿namespace Rasp.PaymentSystems
{
    public class PaymentSystem3 : IPaymentSystem
    {

        private const int SecretKey = 101;

        private Cryptography _cryptography;

        public PaymentSystem3(Cryptography cryptography)
        {
            _cryptography = cryptography;
        }

        public string GetPayingLink(Order order)
        {
            string sha1Hash = _cryptography.GetSha1Hash(order.Amount + order.Id + SecretKey);
            return $"system3.com/pay?amount={order.Amount}&curency=RUB&hash={sha1Hash}";
        }

    }
}
