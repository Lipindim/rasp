﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Rasp.PaymentSystems
{
    public class Cryptography
    {

        public string GetMD5Hash(int source)
        {
            using (var md5 = MD5.Create())
            {
                string hash = String.Concat(md5.ComputeHash(BitConverter
                  .GetBytes(source))
                  .Select(x => x.ToString("x2")));
                return hash;
            }
        }

        public string GetSha1Hash(int source)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(BitConverter.GetBytes(source));
                var stringBuilder = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                    stringBuilder.Append(b.ToString("X2"));

                return stringBuilder.ToString();
            }
        }

    }
}
