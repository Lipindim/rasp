﻿namespace Rasp.PaymentSystems
{
    public class PaymentSystem1 : IPaymentSystem
    {

        private Cryptography _cryptography;

        public PaymentSystem1(Cryptography cryptography)
        {
            _cryptography = cryptography;
        }

        public string GetPayingLink(Order order)
        {
            string md5Hash = _cryptography.GetMD5Hash(order.Id);
            return $"pay.system1.ru/order?amount={order.Amount}RUB&hash={md5Hash}";
        }

    }
}
