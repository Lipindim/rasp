﻿using System;

namespace Rasp.PaymentSystems
{
    class Program
    {
        static void Main()
        {
            var cryptography = new Cryptography();
            var paymentSystems = new IPaymentSystem[]
                {
                    new PaymentSystem1(cryptography),
                    new PaymentSystem2(cryptography),
                    new PaymentSystem3(cryptography)
                };

            var order = new Order(1, 100);

            for (int i = 0; i < paymentSystems.Length; i++)
                Console.WriteLine($"Payment system {i}, link: {paymentSystems[i].GetPayingLink(order)}");

        }
    }
}
