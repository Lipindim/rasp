﻿namespace Rasp.PaymentSystems
{
    public class PaymentSystem2 : IPaymentSystem
    {

        private Cryptography _cryptography;

        public PaymentSystem2(Cryptography cryptography)
        {
            _cryptography = cryptography;
        }

        public string GetPayingLink(Order order)
        {
            string md5Hash = _cryptography.GetMD5Hash(order.Id + order.Amount);
            return $"order.system2.ru/pay?hash={md5Hash}";
        }

    }
}
