﻿using System;

namespace Rasp.Polymorphism
{
    public class CardPaymentSystem : IPaymentSystem
    {
        public void RedirectToPaymentPage()
        {
            Console.WriteLine("Вызов API банка эмитера карты Card...");
        }

        public bool VerifyPayment()
        {
            Console.WriteLine("Проверка платежа через Card...");
            return true;
        }
    }
}
