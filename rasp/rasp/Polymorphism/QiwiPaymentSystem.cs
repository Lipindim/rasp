﻿using System;

namespace Rasp.Polymorphism
{
    public class QiwiPaymentSystem : IPaymentSystem
    {
        public void RedirectToPaymentPage()
        {
            Console.WriteLine("Перевод на страницу QIWI...");
        }

        public bool VerifyPayment()
        {
            Console.WriteLine("Проверка платежа через QIWI...");
            return true;
        }
    }
}
