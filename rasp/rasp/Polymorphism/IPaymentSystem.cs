﻿namespace Rasp.Polymorphism
{
    public interface IPaymentSystem
    {
        void RedirectToPaymentPage();
        bool VerifyPayment();
    }
}
