﻿using System;

namespace Rasp.Polymorphism
{
    public class PaymentHandler
    {
        private IPaymentSystem _paymentSystem;

        public PaymentHandler(IPaymentSystem paymentSystem)
        {
            _paymentSystem = paymentSystem;
        }

        public void ShowPaymentResult()
        {
            _paymentSystem.VerifyPayment();
            Console.WriteLine("Оплата прошла успешно!");
        }
    }
}
