﻿namespace Rasp.Polymorphism
{
    public class PaymentSystemConstsants
    {
        public const string QIWI = "QIWI";
        public const string WebMoney = "WebMoney";
        public const string Card = "Card";
    }
}
