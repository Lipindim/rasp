﻿using System;

namespace Rasp.Polymorphism
{
    public class PaymentSystemFactory
    {
        public IPaymentSystem CreatePaymentSystem(string paymentSystemId)
        {
            if (paymentSystemId == PaymentSystemConstsants.QIWI)
                return new QiwiPaymentSystem();
            else if (paymentSystemId == PaymentSystemConstsants.WebMoney)
                return new WebMoneyPaymentSystem();
            else if (paymentSystemId == PaymentSystemConstsants.Card)
                return new CardPaymentSystem();
            else
                throw new InvalidOperationException($"Unknown payment system {paymentSystemId}");
        }
    }
}
