﻿namespace Rasp.Polymorphism
{
    class Progmar
    {
        static void Main(string[] args)
        {
            var orderForm = new OrderForm();
            string paymentSystemId = orderForm.ShowForm();

            var paymentSystemFactory = new PaymentSystemFactory();
            IPaymentSystem paymentSystem = paymentSystemFactory.CreatePaymentSystem(paymentSystemId);

            paymentSystem.RedirectToPaymentPage();

            var paymentHandler = new PaymentHandler(paymentSystem);
            paymentHandler.ShowPaymentResult();
        }
    }
}
