﻿using System;

namespace Rasp.Polymorphism
{
    public class WebMoneyPaymentSystem : IPaymentSystem
    {
        public void RedirectToPaymentPage()
        {
            Console.WriteLine("Вызов API WebMoney...");
        }

        public bool VerifyPayment()
        {
            Console.WriteLine("Проверка платежа через WebMoney...");
            return true;
        }
    }
}
